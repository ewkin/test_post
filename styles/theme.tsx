const theme = {
    colors: {
        primary: '#0070f3',
        grey: '#eeeeee',
        greyLight: '#f7f7f7',
        greyDark: '#c9c9c9',
        greyTxt: '#565555',
        lemon: '#e8e681',
        black: '#000000',
        blackLight: '#333333',
        red: '#df1f26',
        white: '#FFFF',
    },
    openSans: 'Open Sans, arial,helvetica,sans-serif',
};

export default theme;
