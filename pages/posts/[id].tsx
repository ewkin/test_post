import React from 'react';
import AppToolBar from '../../components/AppToolBar';
import Link from 'next/link';
import { wrapper } from '../../store/configureStore';
import { fetchPost } from '../../store/actions';
import { END } from 'redux-saga';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

const Post = () => {
    const post = useSelector((state) => state.post);

    const Title = styled.h1`
        margin: 0;
    `;

    return (
        <AppToolBar title={'Home'}>
            <Title>{post?.title}</Title>
            <hr />
            <p>{post?.body}</p>
            <Link href={'/'}>
                <a>Back to all posts</a>
            </Link>
        </AppToolBar>
    );
};

export const getServerSideProps = wrapper.getServerSideProps(async ({ store, query, req, res, ...etc }) => {
    if (!store.getState().post) {
        store.dispatch(fetchPost(query.id));
        store.dispatch(END);
    }
    await store.sagaTask.toPromise();
});

export default Post;
