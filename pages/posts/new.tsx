import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import AppToolBar from '../../components/AppToolBar';
import styled from 'styled-components';
import { addPost } from '../../store/actions';

const ContainerFrom = styled.form`
    margin: 0 -15px 0px;
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    justify-content: center;
`;

const Input = styled.input`
    background-color: white;
    border: 1px solid ${({ theme }) => theme.colors.blackLight};
    border-radius: 4px;
    padding-left: 10px;
    margin: 10px 0;
`;
const TextArea = styled.textarea`
    background-color: white;
    border: 1px solid ${({ theme }) => theme.colors.blackLight};
    border-radius: 4px;
    margin: 10px 0;
    padding-left: 10px;
`;

const SubmitButton = styled.button`
    background-color: ${({ theme }) => theme.colors.blackLight};
    border: none;
    color: white;
    margin: 20px 0;
    border-radius: 4px;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    :hover {
        cursor: pointer;
    }
    :focus {
        background-color: ${({ theme }) => theme.colors.black};
    }
`;

const New = () => {
    const dispatch = useDispatch();

    const [post, setPost] = useState({
        title: '',
        body: '',
    });

    const inputChangeHandler = (e: React.ChangeEvent<HTMLInputElement> | React.ChangeEvent<HTMLTextAreaElement>) => {
        const { name, value } = e.target;
        setPost((prev) => ({ ...prev, [name]: value }));
    };

    const submitFormHandler = (e: React.FormEvent) => {
        e.preventDefault();
        console.log(post);
        dispatch(addPost({ ...post }));
    };

    return (
        <AppToolBar title={'New post'}>
            <ContainerFrom onSubmit={submitFormHandler}>
                <label htmlFor="title">Title:</label>
                <Input
                    required
                    onChange={inputChangeHandler}
                    name={'title'}
                    id={'title'}
                    type="text"
                    placeholder={'Post title'}
                    value={post.title}
                />
                <label htmlFor="body">Your post:</label>
                <TextArea
                    required
                    onChange={inputChangeHandler}
                    name={'body'}
                    id={'body'}
                    rows={7}
                    cols={33}
                    placeholder={'Type your post'}
                    value={post.body}
                />
                <SubmitButton type="submit">Submit</SubmitButton>
            </ContainerFrom>
        </AppToolBar>
    );
};

export default New;
