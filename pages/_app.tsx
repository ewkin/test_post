import type { AppProps } from 'next/app';
import { wrapper } from '../store/configureStore';
import NextNprogress from 'nextjs-progressbar';
import { ThemeProvider } from 'styled-components';
import GlobalStyle from '../styles/globalStyles';
import theme from '../styles/theme';

const MyApp = ({ Component, pageProps }: AppProps): JSX.Element => {
    return (
        <>
            <GlobalStyle />
            <ThemeProvider theme={theme}>
                <NextNprogress color="black" startPosition={0.3} stopDelayMs={200} height={4} showOnShallow={true} />
                <Component {...pageProps} />
            </ThemeProvider>
        </>
    );
};
export default wrapper.withRedux(MyApp);
