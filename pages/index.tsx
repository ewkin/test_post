import type { NextPage } from 'next';
import { useSelector, useDispatch } from 'react-redux';

import AppToolBar from '../components/AppToolBar';
import { END } from 'redux-saga';
import { wrapper } from '../store/configureStore';
import { fetchPosts } from '../store/actions';
import styled from 'styled-components';
import Post from '../components/Post';
import { useEffect } from 'react';
import { post } from '../interfaces/intefaces';

const Home: NextPage = () => {
    const dispatch = useDispatch();
    const posts = useSelector((state) => state.posts);

    useEffect(() => {
        if (!posts) {
            dispatch(fetchPosts());
        }
    }, [dispatch]);

    const Container = styled.div`
        margin: 0 -15px 0px;
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
    `;

    return (
        <AppToolBar title={'Home'}>
            <Container>
                {posts ? (
                    posts.map((post: post) => {
                        if (post.body && post.title) {
                            return <Post key={post.id} title={post.title} body={post.body} id={post.id} />;
                        } else {
                            return;
                        }
                    })
                ) : (
                    <p>loading...</p>
                )}
            </Container>
        </AppToolBar>
    );
};

export const getServerSideProps = wrapper.getServerSideProps(async ({ store, req, res, ...etc }) => {
    if (!store.getState().posts) {
        store.dispatch(fetchPosts());
        store.dispatch(END);
    }
    await store.sagaTask.toPromise();
});

export default Home;
