import React from 'react';
import Head from 'next/head';
import Link from 'next/link';
import styled from 'styled-components';

type Props = {
    title?: string;
    children?: React.ReactChild | React.ReactChild[];
};

const Header = styled.header`
    display: flex;
    padding: 0 25px 25px;
    justify-content: space-between;
    height: 50px;
    background: ${({ theme }) => theme.colors.greyLight};
`;
const Container = styled.div`
    padding: 0 25px;
    min-height: 100vh;
    background: ${({ theme }) => theme.colors.greyLight};
`;

const Logo = styled.a`
    align-self: center;
    font-size: 30px;
    color: ${({ theme }) => theme.colors.blackLight};
    font-weight: 200;
    font-family: ${({ theme }) => theme.openSans};
    text-decoration: none;
    :hover {
        cursor: pointer;
    }
    span {
        color: ${({ theme }) => theme.colors.black};
        font-weight: 800;
    }
`;

const NavHolder = styled.div`
    display: flex;
    justify-content: space-between;
    width: 40%;
`;

const NavLink = styled.a`
    text-align: center;
    margin: auto;
    width: 50%;
    height: 30px;
    line-height: 30px;
    border-radius: 25px;
    padding: 0 15px;
    color: ${({ theme }) => theme.colors.blackLight};
    :hover {
        background: ${({ theme }) => theme.colors.blackLight};
        color: ${({ theme }) => theme.colors.greyLight};
        cursor: pointer;
    }
`;

const AppToolBar = ({ children, title = 'Test' }: Props): JSX.Element => {
    return (
        <>
            <Head>
                <title>{title} | Test</title>
                <meta name="keywords" content="tech,javascript,leisure,hobby,coding" />
                <meta name="description" content="this is just a test blog for develops.today" />
                <meta charSet="utf-8" />
            </Head>
            <Header>
                <Link href={'/'}>
                    <Logo>
                        {' '}
                        my<span>blog</span>
                    </Logo>
                </Link>
                <NavHolder>
                    <Link href={'/'}>
                        <NavLink>All posts</NavLink>
                    </Link>
                    <Link href={'/posts/new'}>
                        <NavLink>Add new post</NavLink>
                    </Link>
                </NavHolder>
            </Header>
            <main>
                <Container>{children}</Container>
            </main>
        </>
    );
};

export default AppToolBar;
