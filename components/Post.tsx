import React from 'react';
import styled from 'styled-components';
import Link from 'next/link';

type Props = {
    title: string;
    body: string;
    id: number;
};

const Post = ({ title, body, id }: Props) => {
    const PostContainer = styled.div`
        padding: 10px 10px;
        width: 25%;
        max-height: 200px;
    `;
    const Post = styled.a`
        background: ${({ theme }) => theme.colors.grey};
        padding-top: 24px;
        padding-bottom: 23px;
        height: 100px;
        display: flex;
        flex-direction: column;
        p {
            font-style: italic;
            font-size: 14px;
            color: ${({ theme }) => theme.colors.greyTxt};
            margin: auto 0 0 26px;
            padding: 0;
            line-height: 1.8;
        }
        div {
            font-size: 16px;
            color: ${({ theme }) => theme.colors.black};
            font-weight: 700;
            margin: auto 0 0 26px;
        }

        :hover {
            background: ${({ theme }) => theme.colors.blackLight};
            cursor: pointer;
            p,
            div {
                color: ${({ theme }) => theme.colors.greyLight};
            }
        }
    `;

    const truncate = (input) => (input.length > 10 ? `${input.substring(0, 5)}...` : input);

    return (
        <PostContainer>
            <Link href={`/posts/[id]`} as={`/posts/${id}`}>
                <Post>
                    <div>{title}</div>
                    <p>{truncate(body)}</p>
                </Post>
            </Link>
        </PostContainer>
    );
};

export default Post;
