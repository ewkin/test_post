export const actionTypes = {
    FETCH_POSTS: 'FETCH_POSTS',
    FETCH_POSTS_SUCCESS: 'FETCH_POSTS_SUCCESS',
    FETCH_POSTS_FAILURE: 'FETCH_POSTS_FAILURE',
    FETCH_POST: 'FETCH_POST',
    FETCH_POST_SUCCESS: 'FETCH_POST_SUCCESS',
    FETCH_POST_FAILURE: 'FETCH_POST_FAILURE',
    ADD_POST: 'ADD_POST',
    ADD_POST_SUCCESS: 'ADD_POST_SUCCESS',
    ADD_POST_FAILURE: 'ADD_POST_FAILURE',
    HYDRATE: 'HYDRATE',
};
import { newPost, post } from './../interfaces/intefaces';

export function fetchPosts() {
    return {
        type: actionTypes.FETCH_POSTS,
    };
}

export function successFetchPosts(data: []) {
    return {
        type: actionTypes.FETCH_POSTS_SUCCESS,
        data,
    };
}

export function failureFetchPosts(error: any) {
    return {
        type: actionTypes.FETCH_POSTS_FAILURE,
        error,
    };
}
export function fetchPost(id: number) {
    return {
        type: actionTypes.FETCH_POST,
        payload: id,
    };
}

export function successFetchPost(data: post) {
    return {
        type: actionTypes.FETCH_POST_SUCCESS,
        data,
    };
}

export function failureFetchPost(error: any) {
    return {
        type: actionTypes.FETCH_POST_FAILURE,
        error,
    };
}

export function addPost(postData: newPost) {
    return {
        type: actionTypes.ADD_POST,
        payload: postData,
    };
}

export function successAddPost() {
    return {
        type: actionTypes.ADD_POST_SUCCESS,
    };
}

export function failureAddPost(error: any) {
    return {
        type: actionTypes.ADD_POST_FAILURE,
        error,
    };
}
