import { put, call, takeLatest } from 'redux-saga/effects';
import axiosApi from '../../axiosApi';
import {
    actionTypes,
    addPost,
    failureAddPost,
    failureFetchPost,
    failureFetchPosts,
    fetchPost,
    successAddPost,
    successFetchPost,
    successFetchPosts,
} from '../actions';

import Router from 'next/router';

import { action } from '../../interfaces/intefaces';

export interface ResponseGenerator {
    config?: any;
    data?: any;
    headers?: any;
    request?: any;
    status?: number;
    payload?: any;
    statusText?: string;
}

export function* postAdd(action: action) {
    try {
        const payload: ResponseGenerator = yield call(addPost, action.payload);
        yield axiosApi.post('/posts', payload.payload);
        yield put(successAddPost());
        Router.push('/');
    } catch (error) {
        yield put(failureAddPost({ error: error }));
    }
}

export function* postsFetch() {
    try {
        const data: ResponseGenerator = yield axiosApi.get('/posts');
        yield put(successFetchPosts(data.data));
    } catch (error) {
        yield put(failureFetchPosts({ error: error }));
    }
}

export function* postFetch(action: action) {
    try {
        const payload: ResponseGenerator = yield call(fetchPost, action.payload);
        const postResponse: ResponseGenerator = yield axiosApi.get(`/posts/${payload.payload}`);
        yield put(successFetchPost(postResponse.data));
    } catch (error) {
        yield put(failureFetchPost({ error: error }));
    }
}

const postsSagas = [
    takeLatest(actionTypes.FETCH_POSTS, postsFetch),
    takeLatest(actionTypes.FETCH_POST, postFetch),
    takeLatest(actionTypes.ADD_POST, postAdd),
];

export default postsSagas;
