import { all } from 'redux-saga/effects';
import postsSagas from './sagas/postsSaga';

export default function* rootSaga() {
    yield all([...postsSagas]);
}
