import { actionTypes } from './actions';
import { HYDRATE } from 'next-redux-wrapper';

const initialState = {
    posts: null,
    post: null,
    error: false,
    loading: false,
};

function reducer(state = initialState, action) {
    switch (action.type) {
        case HYDRATE: {
            return { ...state, ...action.payload };
        }
        case actionTypes.FETCH_POSTS:
            return {
                ...state,
                ...{ loading: true },
            };
        case actionTypes.FETCH_POSTS_SUCCESS:
            return {
                ...state,
                ...{ posts: action.data },
                ...{ loading: false },
            };
        case actionTypes.FETCH_POSTS_FAILURE:
            return {
                ...state,
                ...{ error: action.error },
                ...{ loading: false },
            };
        case actionTypes.FETCH_POST:
            return {
                ...state,
                ...{ loading: true },
            };
        case actionTypes.FETCH_POST_SUCCESS:
            return {
                ...state,
                ...{ post: action.data },
                ...{ loading: false },
            };
        case actionTypes.FETCH_POST_FAILURE:
            return {
                ...state,
                ...{ error: action.error },
                ...{ loading: false },
            };
        case actionTypes.ADD_POST:
            return {
                ...state,
                ...{ loading: true },
            };
        case actionTypes.ADD_POST_SUCCESS:
            return {
                ...state,
                ...{ loading: false },
            };
        case actionTypes.ADD_POST_FAILURE:
            return {
                ...state,
                ...{ error: action.error },
                ...{ loading: false },
            };

        default:
            return state;
    }
}

export default reducer;
